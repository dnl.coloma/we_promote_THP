# Notre Synthèse

## Stratégie d'ensemble

Nous avons commencé la journée en définissant les segments de prospects potentiels que nous pouvions cibler, et nous en avons choisi deux en fonction de:
1) Pertinence de la cible
2) Efficacité de l'acquisition (est-ce simple d'automatiser cette tache?)

Nous avons retenu deux segments, et créé une landing pour chacun, et une méthode d'acquisition:

Cible | Raisonnement
--- |---
Ecoles (commerce ou d'ingénieur) | Susceptibles de faire un partenariat avec THP ou de communiquer auprès de leurs étudiants.
Followers Twitter du Wagon | Prospects ayant déjà manifesté un intérêt pour l'apprentissage du code.

**Nos deux landings:**
- [Landing pour les followers du Wagon](https://apprenez-le-code-avec-thp.herokuapp.com/aspirants)
- [Landing pour les écoles](https://apprenez-le-code-avec-thp.herokuapp.com/schools)

## Conception des landings

Nous avons utilisé deux templates différents. Chacun est adapté à la cible visée. Ils présentent néanmoins les similitudes suivantes:
* Un style épuré qui ne va pas noyer l'information dans la page
* une hiérarchisation et une présentation ludique de l'information
* Des couleurs et une police sobres pour un rendu professionnel
* Une mise en avant du call to action

Tout les assets des templates ont été stocké dans le dossier `lib`. Pour la gestion des stylesheets dans l'asset pipeline, nous avons ajouté un yield dans le `<head>` de `app/views/layouts/application.html.erb` afin que chaque view utilise sa stylesheet propre et ne mélange pas les styles avec l'autre.

## Configuration mailchimp

Le call to action renvoie vers l'API de MailChimp.
- On a créé un compte mailchimp
- On a récupéré la clé API et on la insérée dans un fichier .env
- On a récupéré la version 'naked', sans styling, pour l'adapter à notre template
- On a testé en enlevant quelques div
- On l'a intégré dans les forms des views en jouant sur les classes css

## Méthodes d'acquisition

Nous avons décidé d'utiliser deux méthodes : une première utilisant du scrapping sur le site de l'ONISEP et une deuxième via l'API twitter.

### Scrapping
Nous nous sommes penchés vers le site de l'ONISEP pour cibler les jeunes dans les écoles post-bac. Ce site recense toutes les formations certifiantes/diplomantes en France. Des listes de contacts sont recensent les noms des écoles, adresses mail et la ville. Il y a une liste par catégorie (ingénieur, commerce). Ces listes sont ensuite envoyés vers MailChimp afin de pouvoir polluer leur boite mail :smiling_imp:

### Bot Twitter
Lien vers le compte twitter : https://twitter.com/bailly_bes?lang=fr .


## Méthodes de tracking et d'analyses

Pour le tracking, nous avons utilisé deux méthodes principales: 
- Installation de Google Analytics sur le site pour mesurer le traffic.
- Ajouts de tags uniques (du type: `aspirants?utm_source=Twitter&utm_medium=social_media&utm_campaign=lewagon`) dans les URL de chaque campagne pour connaître la provenance du traffic et comprendre quelles campagnes ont été les plus efficaces. 

Pour la conversion, on n'a pas eu le temps de mettre en place un funnel pour mesurer le nombre de clics sur notre bouton subscribe donc notre méthode sera nombre de subscribers mailchimp / nombre de visites google analytics (pour mesurer le taux de conversion). 

## Résultats

A suivre dans la prochain épisode :wink: 

## Pistes d'amélioration

### 1- Vitesse de chargement de notre landing page
La hantise des équipes web, c'est que le visiteur quitte le site parce que celui-ci met trop de temps à charger. Pour tester la rapidité de votre page, Google propose un outil de diagnostic gratuit. [L'outil est disponible ici](https://developers.google.com/speed/pagespeed/insights/)

### 2- Référencement
Pour être référencé sur Google, c'est par ici. [LE SEO](https://support.google.com/webmasters/answer/7451184?hl=en)
Pour ressortir dans les recherches par mots clés c'est par là : [Google Adwords](https://adwords.google.com/KeywordPlanner)

NB: quelques soucis avec le Bot twitter (mises à jour twitter et nombre de requêtes autorisés pour les APIs) ; nous travaillons encore dessus pour corriger ce problème. 
A suivre dans l'épisode suivant le prochain épisode :wink::wink: 
