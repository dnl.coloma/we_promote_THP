require 'nokogiri'
require 'open-uri'
require 'pry'
require 'gibbon'
require 'dotenv'
require 'csv'

Dotenv.load

class Scrapping
  @@i=1
    #récupère l'adresse email à partir de l'url d'une mairie
  def get_the_email(url)

  page = Nokogiri::HTML(open(url))
  #recherche par CSS la section Adresse email
  page.css(".email").text

  end


  def action_by_page(page,list_school)
    #Recherche tous le code html des villes
    list_url = page.xpath('//tbody/tr/td/a')
    list_city = page.xpath('//table/tbody/tr/td[2]')
    list_url.each do |link|
      infos = []
      #compléter l'URL de chaque école
      url = "http://www.onisep.fr"+link['href']
      school_name = link.text
      city = list_city[list_url.index(link)].text

      infos << school_name.strip << city.strip
      #Hash{ Ville => [URL_ecole, email_école]}
      if get_the_email(url)
        list_school[get_the_email(url)] = infos
      end
    end

    list_school
  end

  def get_all_the_urls_of_schools(url)
    i=1
    page = Nokogiri::HTML(open(url))
    a = page.css('span[class="next"] > a')[0]
    domain ="http://www.onisep.fr"
    list_school={}
    puts "Getting URLs from ONISEP..."
    puts "Page #{i} loading..."
    action_by_page(page,list_school)
    puts "Page #{i} done!"
    while page.css('span[class="next"] > a')[0]
      i += 1
      puts "Page #{i} loading..."
      a = page.css('span[class="next"] > a')[0]
      page = Nokogiri::HTML(open(domain + a['href']))
      action_by_page(page,list_school)
      puts "Page #{i} done!"

    end
=begin
    list_school.each do |email,infos|
      puts "#{infos[0]} - #{infos[1]} : #{email}"
    end
=end
    puts "All page done !"
    list_school
  end

  def put_in_csv(get_hash)
    CSV.open("contacts#{@@i}.csv", "wb") do |csv|
      get_hash.each do |key,value|
        csv << [key,value]
      end

    end
    @@i += 1
    puts "CSV done!"
  end

  def create_contacts_list
    gibbon = Gibbon::Request.new(api_key: ENV['MAILCHIMP_API_KEY'])
    i=1
    contacts=[]
    while i<2
      array=[]
      CSV.foreach("contacts2.csv") do |row|
        array << row
      end
      i += 1
      array.each do |row|

        contacts << row[0]
      end

    end
=begin
    contacts.each do |k,v|
      puts "#{k} : #{v}"
    end
=end
    contacts.each do |email|

      if email.length > 0

        email.split(" ou ").each do |e|
          p e
          gibbon.lists('b965fa5d7a').members.create(body: {email_address: e, status: "subscribed"})
        end
      end

    end
    puts "Done!"
  end

end

ECOLE_COMMERCE="http://www.onisep.fr/content/search?SubTreeArray=243418&etabRecherche=1&idFormation=&limit=50&filters[attr_categorie_type_etablissement_t][]=11"

ECOLE_INGENIEUR="http://www.onisep.fr/content/search?SubTreeArray=243418&etabRecherche=1&limit=50&filters[attr_categorie_type_etablissement_t][]=4"

ECOLE_ARTS = "http://www.onisep.fr/content/search?SubTreeArray=243418&etabRecherche=1&limit=50&filters[attr_categorie_type_etablissement_t][]=2"

schools = [ECOLE_INGENIEUR,ECOLE_COMMERCE]#,ECOLE_ARTS]
scrap = Scrapping.new
#contacts = []

=begin
schools.each do |school|
  #contacts << scrap.get_all_the_urls_of_schools(school)
  scrap.put_in_csv(scrap.get_all_the_urls_of_schools(school))
end
puts "Contacts list done !"
=end

scrap.create_contacts_list
=begin
contacts.each do |school|
  scrap.put_in_csv(school)
  puts "List Done with success !"
end
=end
