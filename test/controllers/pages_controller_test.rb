require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  test "should get landing_wagon" do
    get pages_landing_wagon_url
    assert_response :success
  end

end
