Rails.application.routes.draw do
  get '/aspirants', to: 'pages#landing_aspirants'
  get '/schools', to: 'pages#landing_schools'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
