# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )

Rails.application.config.assets.precompile += %w( BigPicture/main.scss )
Rails.application.config.assets.precompile += %w( SuperAwesome/styles.scss )


Rails.application.config.assets.precompile += %w( BigPicture/thumbs/01.jpg )
Rails.application.config.assets.precompile += %w( BigPicture/thumbs/02.jpg )
Rails.application.config.assets.precompile += %w( BigPicture/thumbs/03.jpg )
Rails.application.config.assets.precompile += %w( BigPicture/thumbs/04.jpg )
Rails.application.config.assets.precompile += %w( BigPicture/thumbs/05.jpg )
Rails.application.config.assets.precompile += %w( BigPicture/thumbs/06.jpg )
Rails.application.config.assets.precompile += %w( SuperAwesome/ap-screenshot.png )
Rails.application.config.assets.precompile += %w( SuperAwesome/screen1.png )
Rails.application.config.assets.precompile += %w( SuperAwesome/the-hacking-project.jpg )

